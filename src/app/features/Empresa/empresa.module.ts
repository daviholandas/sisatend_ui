import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SharedModule } from 'src/app/shared/shared.module';
import { EmpresaLayoutComponent } from './layout/empresa-layout.component';
import { EmpresasComponent } from './pages/empresas/empresas.component';
import { EmpresaRoutingModule } from './empresa-routing.module';
import { EmpresasService } from 'src/app/services/empresas.service';
import { InfoComponent } from './components/info/info.component';

@NgModule({
  declarations: [EmpresaLayoutComponent, EmpresasComponent, InfoComponent],
  imports: [
    CommonModule,
    SharedModule,
    EmpresaRoutingModule,
    FormsModule
  ],
  exports:[EmpresasComponent, InfoComponent],
  providers:[EmpresasService]
})
export class EmpresaModule { }
