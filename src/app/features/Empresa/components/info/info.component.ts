import { Component, OnInit, Input } from '@angular/core';
import { Empresa } from 'src/app/interfaces/empresa';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  @Input() empresa: Empresa;

  constructor( ) { }

  ngOnInit() {
  }

}
