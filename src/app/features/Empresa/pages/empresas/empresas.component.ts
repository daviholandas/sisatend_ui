import { Component, OnInit } from '@angular/core';
import { EmpresasService } from 'src/app/services/empresas.service';
import { Empresa } from 'src/app/interfaces/empresa';

import { ClrDatagridFilterInterface } from 'src/app/interfaces/filter'
import { Subject } from 'rxjs';


class MyFilter implements ClrDatagridFilterInterface<Empresa> {
  changes = new Subject<any>();
  accepts(empresa: Empresa) { return false; }
  isActive(): boolean { return false; }
}



@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.css']
})
export class EmpresasComponent implements OnInit {
  public empresas: Empresa[] = [];
  public empresa: Empresa;
  protected nomeEmpresa: string;
  protected pageSize = 20;
  protected lastPage: number;
  protected totalItems: number;
  public terms: string;
  public myFilter = new MyFilter();
  public dialog: boolean;
  private currentPage: number;

  constructor(private empresasService: EmpresasService) {
    this.dialog = false;
  }



  getValores(empresa: Empresa) {
    this.empresa = empresa;
    this.dialog = true;
    this.nomeEmpresa = empresa.NOME;
    console.log(empresa);
  }

  getEmpresas(currentPage: number = 1): void {
    this.empresasService.getEmpresas(currentPage).subscribe(resp => {
      this.lastPage = resp.last_page;
      this.totalItems = resp.total;
      this.empresas = resp.data;
    });
  }

  filterEmpresas(empresas: Empresa[], nomeEmpresa: string) {
    empresas.map(empresa => console.log(empresa));
  }

  changePage(page){
   this.getEmpresas(page);
  }

  ngOnInit() {
    this.getEmpresas(this.currentPage);
    this.filterEmpresas(this.empresas, this.terms);
  }

}
