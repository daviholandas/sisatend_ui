import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClarityModule} from '@clr/angular';

//Componentes
import { HomeComponent } from './components/home/home.component';
import { FormsModule } from '@angular/forms';



@NgModule({
    declarations: [HomeComponent],
    imports: [ CommonModule, ClarityModule, FormsModule  ],
    exports: [ClarityModule, HomeComponent],
    providers: [],
})
export class SharedModule{}