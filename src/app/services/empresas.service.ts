import { Injectable } from '@angular/core';
import { Empresa } from '../interfaces/empresa';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class EmpresasService {

  constructor(private http:HttpClient) { }

  getEmpresas(curretPage:number): Observable<any>{
    return this.http.get<any>(environment.API_HOST + `/empresas?page=${curretPage}`)
    
  }

  getRiscos(empresa:Empresa):Observable<any[]>{
    return 
  }
}
