import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './shared/components/home/home.component';
//Layouts


const routes: Routes = [
  {path:'', component: HomeComponent},
  {path:'empresa', loadChildren:'./features/Empresa/empresa.module#EmpresaModule'},
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
