export interface Empresa {
    CODIGO:number;
    NOME:string;
    RAZAO:string;
    CNPJ:string
}
