# Sisatend Reborn

Essa é a parte visual do projeto, estar sendo feito em Angular. Uso é somente para consumo de dados pois o banco estar inativo.
## Tecnologias usadas

### Backend
PHP com Laravel.
- Foi usado essa tecnologia por conta que o banco de dados é firebird a unica linguagem não fortimente tipada que tem um drive para o firebird é PHP;
- Posso usar padrões de projetos e até OOP com PHP sem a necessidade do mapeamento da entidades relacionais;

### Frontend
Angular 8 com a libary ui Clarity Design;
- Está sendo usado o Angular para aprendizado de um novo framework;
- A libary é usada por conta que não tenho facilidade e nem skills para trabalhar com HTML e CSS e também pelo fator tempo;

## Empresa RISETECH

Desenvolvido por Davi Holanda